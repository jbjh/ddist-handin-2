import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.*;
import javax.swing.text.*;

public class DistributedTextEditor extends JFrame {
	//useing our own document implementation allows us to have a boolean that handles if your allowed to edit the
	//area field or not this is used to handle the infinite loop that else would occurs
	private JTextArea myJTextAreaInput = new JTextArea("",20,60); 
	private JTextField ipAddressfield = new JTextField("IP address here");     
	private JTextField portNumberfield = new JTextField("portnumber here");  
	private JFileChooser dialog = new JFileChooser(System.getProperty("user.dir"));
	private String currentFile = "Untitled";
	private HistoryHandler myHistoryHandler = new HistoryHandler();

	protected int defaultPortNumber = 40304;
	protected int portNumber;
	private DocumentEventCapturer myDocumentEventCapturer = new DocumentEventCapturer();
	private EventReplayer myEventReplayer;
	private EventSender myEventTransmitter;
	private Thread myEventTransmitterThread;
	private Thread myEventReplayThread;   

	
	protected boolean listen;
	private boolean connected;
	private boolean changed = false;
	
	ActionMap m = myJTextAreaInput.getActionMap();
	
	Action Copy = m.get(DefaultEditorKit.copyAction);
	Action Paste = m.get(DefaultEditorKit.pasteAction);
	InetAddress localhost;
	protected ServerSocket serverSocket;
	protected Socket clientSocket;
	
	/**
	 * Main constructor
	 * Creates the main GUI
	 */
	public DistributedTextEditor() {
		myJTextAreaInput.setDocument(new TextAreaDocument());
		myJTextAreaInput.setFont(new Font("Monospaced",Font.PLAIN,12));
	
		myJTextAreaInput.addKeyListener(k1);
		((AbstractDocument)myJTextAreaInput.getDocument()).setDocumentFilter(myDocumentEventCapturer);
		Container content = getContentPane();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

		JScrollPane scroll1 = 
				new JScrollPane(myJTextAreaInput, 
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		content.add(scroll1,BorderLayout.CENTER);

		content.add(ipAddressfield,BorderLayout.CENTER);	
		content.add(portNumberfield,BorderLayout.CENTER);	

		JMenuBar JMB = new JMenuBar();
		setJMenuBar(JMB);
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMB.add(file); 
		JMB.add(edit);

		file.add(Listen);
		file.add(Connect);
		file.add(Disconnect);
		file.addSeparator();
		file.add(Save);
		file.add(SaveAs);
		file.add(Quit);

		edit.add(Copy);
		edit.add(Paste);
		edit.getItem(0).setText("Copy");
		edit.getItem(1).setText("Paste");

		Save.setEnabled(false);
		SaveAs.setEnabled(false);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		
		setTitle("Disconnected");
		setVisible(true);
	}
	
	/**
	 * KeyListener attached to the input JTextArea.
	 */
	private KeyListener k1 = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			changed = true;
			Save.setEnabled(true);
			SaveAs.setEnabled(true);
		}
	};
	
	/**
	 * Helper method 
	 * Sets port number, to specified or reverts to default.
	 * @NumberFormatException - Thrown if no Integer can be parsed in the portNumberField.
	 */ 
	private void setPortNumber (){
		try{
			portNumber = Integer.parseInt(portNumberfield.getText()); //Throws NumberFormatException.
			if(portNumber > 65535 || portNumber < 1){ //Checks if portNumber is in range of the value limits.
				portNumber = defaultPortNumber; //Reverts to default port.
			}
		} catch (NumberFormatException nfe){
//			nfe.printStackTrace();
			portNumber = defaultPortNumber; //Reverts to default port.
		}
	}
	
	/**
	 * Helper method
	 * Clears input and output text areas.
	 */
	private void clearTextField(){
		myJTextAreaInput.setText("");
	}
	
	/**
	 * Thread starter method, starts a new thread of the EventReplayer class.
	 * @param socket, containing the socket from connecting or listening.
	 */
	protected void startEventReplayer(Socket socket) throws IOException{
		
		//outputStream = new ObjectOutputStream(socket.getOutputStream());
		myEventReplayer = new EventReplayer(myJTextAreaInput, socket, myHistoryHandler);
		myEventReplayThread = new Thread(myEventReplayer);
		myDocumentEventCapturer.clearHistory();
		myEventReplayThread.start();
		myEventTransmitter = new EventSender(myDocumentEventCapturer, socket, myHistoryHandler);
		myEventTransmitterThread = new Thread(myEventTransmitter);
		myEventTransmitterThread.start();
		checkForDisconnect();
		connected = true;
	}
	
	/**
	 * Method to check for disconnects, it starts a new Thread to run in the background 
	 * checking if the socket failed at some point. If it did it recovers.
	 */
	protected void checkForDisconnect(){
		Runnable disconnectsRunnable = new Runnable(){
			public void run() {
				while(connected){
					myEventReplayThread.isAlive();
					if(myEventReplayer.checkSocket()){ //Checks if the EventReplayer had a socket exception.
						clearTextField();
						if(serverSocket != null){
							if(serverSocket.isClosed()){
								Disconnect.actionPerformed(null); //Forces a disconnect call to clear all sockets and threads.
							}
							else if (!serverSocket.isClosed()){
								setTitle("I'm listening on "+localhost.getHostAddress()+":"+portNumber
								+" - Client disconnected");
							}
							connected = false;
						} else if (clientSocket != null){
							Disconnect.actionPerformed(null); //Forces a disconnect call to clear all sockets and threads.
							connected = false;
						}
					}
				}
			}
		};
		Thread disconnectsThread = new Thread(disconnectsRunnable); //Creates a new thread.
		disconnectsThread.start(); //Starts the new thread.
	}

	/**
	 * Menu item - Listen
	 */
	Action Listen = new AbstractAction("Listen") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			clearTextField();
			listen = true;
			try {
				localhost = InetAddress.getLocalHost(); //Retrieves the local machines IP address.
				setPortNumber();
				serverSocket = new ServerSocket(portNumber); //Creates a new socket for the server.
				setTitle("I'm listening on "+localhost.getHostAddress()+":"+portNumber);	
				myHistoryHandler.setIndex(0);
			} 
			catch(BindException bind){ 
				setTitle("Disconnected - Try another port");
//				bind.printStackTrace();
			} 
			catch (Exception ex) {
//				ex.printStackTrace();
			} 	
			changed = false;
			Save.setEnabled(false);
			SaveAs.setEnabled(false);

			new Thread(){ //New thread to listen for incoming connections.
				public void run(){
					while(listen){ //Loops back to listen again, even if a connection is made. Used to recover from failed connections.
						Socket socket;
						try{
							socket = serverSocket.accept(); //Waits for incoming connection. Blocks until a connection is made.
							if(socket != null){
							clearTextField();
							setTitle("I'm listening on  "
                                    +localhost.getHostAddress()
                                    +":"+portNumber 
                                    +" Client connected from "
                                    +socket.getRemoteSocketAddress());
							System.out.println("SERVER");
							startEventReplayer(socket);
							}
						}
						catch(Exception ex){
//							ex.printStackTrace();
							listen = false; //Stops listening if any exception is thrown.
						}
					}
				}
			}.start();
		}
	};

	/**
	 * Menu item - Connect
	 */
	Action Connect = new AbstractAction("Connect") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			clearTextField();
			setPortNumber();
			setTitle("Connecting to " + ipAddressfield.getText() + ":" + portNumber + "...");
			changed = false;
			Save.setEnabled(false);
			SaveAs.setEnabled(false);
			
			try{		
				clientSocket = new Socket(ipAddressfield.getText(), portNumber); //Creates a new socket for the client connection.
				if(clientSocket.isConnected()){ //Checks if the socket is connected.
					myHistoryHandler.setIndex(1);
					setTitle("Connected to " + ipAddressfield.getText() + ":" + portNumber);
					System.out.println("Client");
					startEventReplayer(clientSocket);
				}
			//Catching exceptions thrown by socket connection issues. Sets title accordingly.
			}catch(UnknownHostException uhe){
				setTitle("Disconnected - Connection failed, try another host");
				uhe.printStackTrace();
			}catch(ConnectException ce){
				setTitle("Disconnected - Connection timed out, try another host");
				ce.printStackTrace();
			}catch(Exception ex){
				setTitle("Disconnected");
				ex.printStackTrace();
			}
		}
	};

	/**
	 * Menu item - Disconnect
	 */
	Action Disconnect = new AbstractAction("Disconnect") {
		public void actionPerformed(ActionEvent e) {	
			setTitle("Disconnected");
			try {
				listen = false;
				if(myEventReplayer != null){ //Checks if eventReplayer is started and closes it if true.
					myEventReplayer.closeThread();					
					myEventReplayThread.interrupt();
				}
				if(clientSocket != null){ //If client socket exists. Close it.
					clientSocket.close();
				}
				if(serverSocket != null){ //If server socket exists. Close it.
					serverSocket.close();
				}
			} catch (Exception ex) { //Catches all exception.
//				ex.printStackTrace();
			}
		}
	};

	/**
	 * Menu item - Save
	 */
	Action Save = new AbstractAction("Save") {
		public void actionPerformed(ActionEvent e) {
			if(!currentFile.equals("Untitled"))
				saveFile(currentFile);
			else
				saveFileAs();
		}
	};

	/**
	 * Menu item - SaveAs
	 */
	Action SaveAs = new AbstractAction("Save as...") {
		public void actionPerformed(ActionEvent e) {
			saveFileAs();
		}
	};

	/**
	 * Menu item - Quit
	 */
	Action Quit = new AbstractAction("Quit") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			System.exit(0);
		}
	};
	
	private void saveFileAs() {
		if(dialog.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
			saveFile(dialog.getSelectedFile().getAbsolutePath());
	}

	private void saveOld() {
		if(changed) {
			if(JOptionPane.showConfirmDialog(this, "Would you like to save "+ currentFile +" ?","Save",JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION)
				saveFile(currentFile);
		}
	}

	private void saveFile(String fileName) {
		try {
			FileWriter w = new FileWriter(fileName);
			myJTextAreaInput.write(w);
			w.close();
			currentFile = fileName;
			changed = false;
			Save.setEnabled(false);
		}
		catch(IOException e) {
		}
	}

	/**
	 * Main method. Initializes the GUI.
	 * @param arg
	 */
	public static void main(String[] arg) {
		new DistributedTextEditor();
	} 
}
