import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class EventSender implements Runnable {

	private HistoryHandler myHistoryHandler;
	private DocumentEventCapturer myDocumentEventCapturer;
	private ObjectOutputStream myOutputStream;

	public EventSender(DocumentEventCapturer dec, Socket s, HistoryHandler hh) {
		this.myDocumentEventCapturer = dec;
		try {
			myOutputStream = new ObjectOutputStream(s.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		myHistoryHandler = hh;
	}

	public void run() {
		boolean wasInterrupted = false;

		while (!wasInterrupted) {
			try {
				MyTextEvent myTextEvent = myDocumentEventCapturer.take();
				myTextEvent.setSender(myHistoryHandler.getIndex());
				myHistoryHandler.incrementTime();
				myTextEvent.setTime(myHistoryHandler.getTime());
				myOutputStream.writeObject(myTextEvent);
				myHistoryHandler.appendEvent(myTextEvent);
				myOutputStream.flush();
			} catch (Exception e){
				wasInterrupted = true;
//				e.printStackTrace();
			}
		}
		System.out.println("I'm the thread running the EventTransmitter, now I die!");
	}
}