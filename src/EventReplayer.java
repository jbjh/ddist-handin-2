import javax.swing.JTextArea;
import javax.swing.text.Document;

import java.awt.EventQueue;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * 
 * Takes the event recorded by the DocumentEventCapturer and replays
 * them in a JTextArea. The delay of 1 sec is only to make the individual
 * steps in the reply visible to humans.
 * 
 * @author Jesper Buus Nielsen
 *
 */
public class EventReplayer implements Runnable {

	private JTextArea myJTextArea;
	private Socket socket;
	private ObjectInputStream myInputStream;
	private boolean wasInterrupted;
	private boolean socketFailed = false;
	private Thread receiverThread;
	private HistoryHandler myHistoryHandler;
	private TextAreaDocument myTextAreaDocument;
	private int ownIndex;

	/**
	 * Constructor for the class
	 * @param area Takes the JTextArea to write to/read from
	 * @param paramSocket Takes the socket used to connect to other computer
	 * @param myHistoryHandler takes a HistoryHandler that handles clock and queue
	 */
	public EventReplayer(JTextArea area, Socket paramSocket, HistoryHandler myHistoryHandler) {
		this.myJTextArea = area;
		this.socket = paramSocket;
		this.myHistoryHandler = myHistoryHandler;
		myTextAreaDocument = (TextAreaDocument) area.getDocument();
	}

	/**
	 * Called to interrupt the innerThread for receiving input.
	 * @throws IOException - Is propagated back to DistributedTextEditor class.
	 */
	protected void closeThread() throws IOException{
		wasInterrupted = true;
		receiverThread.interrupt();
		socket.close();
	}
	/**
	 * Check if socket failed
	 */
	public boolean checkSocket(){
		return socketFailed;
	}

	/**
	 * The thread to run, enables receiving input
	 */
	public void run() {
		wasInterrupted = false; //Variable to check if interrupted.
		try {
			myInputStream = new ObjectInputStream(socket.getInputStream());
			while (!wasInterrupted){
				final MyTextEvent myTextEvent = (MyTextEvent)myInputStream.readObject(); //Reads the incoming objects.
				final ArrayList<Double> time = myTextEvent.getTime();
				final int fromIndex = myTextEvent.getSender();
				
				//Code for handling text insert events
				if (myTextEvent instanceof TextInsertEvent) { 
					final TextInsertEvent myTextInsertEvent = (TextInsertEvent) myTextEvent;
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								synchronized(myTextAreaDocument){
									boolean skip = false; 
									ArrayList<MyTextEvent> myTextEventHistory = sortEventHistory(time, fromIndex);
									//Going through the eventqueue
									for(MyTextEvent my : myTextEventHistory){
										int listObjectOffset = my.getOffset();
										int listObjectChange = my.getChange();
										int myTextInsertEventOffset = myTextInsertEvent.getOffset();
										if(listObjectOffset <= myTextInsertEventOffset && (listObjectOffset != myTextInsertEventOffset || ownIndex < fromIndex )){
											if(my instanceof TextRemoveEvent && listObjectOffset + ((TextRemoveEvent) my).getLength() > myTextInsertEventOffset){
												skip = true;
												((TextRemoveEvent) my).setLength(((TextRemoveEvent) my).getLength() + myTextInsertEvent.getChange());
											}else{myTextInsertEvent.setOffset(myTextInsertEventOffset + listObjectChange);}
										}else{my.setOffset(listObjectOffset + myTextInsertEvent.getChange());}
									}
									myHistoryHandler.updateLogicClock(time);
									
									//for writing received input without retransmitting it
									if(!skip){
										getAreaDocument().flipFilter();
										myJTextArea.insert(myTextInsertEvent.getText(), myTextInsertEvent.getOffset());
										int pos = myJTextArea.getCaret().getDot();
										if(myTextEvent.getOffset() == pos && fromIndex < ownIndex){
											myJTextArea.getCaret().setDot(pos);
										}
										getAreaDocument().flipFilter();
									}
								}
							}catch (Exception ex) {
								//	ex.printStackTrace();
							}
						}
					});
					//Code for handling text remove events
				} else if (myTextEvent instanceof TextRemoveEvent) { 
					final TextRemoveEvent myTextRemoveEvent = (TextRemoveEvent) myTextEvent;
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								synchronized (myTextAreaDocument) {
									boolean skip = false;
									ArrayList<MyTextEvent> myTextEventHistory = sortEventHistory(time, fromIndex);
									//Going through the eventqueue
									for (MyTextEvent historyItem : myTextEventHistory) {
										int removeEventOffset = myTextRemoveEvent.getOffset();
										int removeEventTextLengthChange = myTextRemoveEvent.getChange();
										int removeEventLength = myTextRemoveEvent.getLength();
										int historyItemOffset = historyItem.getOffset();
										int historyItemTextLengthChange = historyItem.getChange();

										if (historyItemOffset <= removeEventOffset && (historyItemOffset != removeEventOffset || ownIndex < fromIndex)) {
											if (historyItem instanceof TextRemoveEvent && removeEventOffset <= historyItemOffset + ((TextRemoveEvent) historyItem).getLength() && (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() != removeEventOffset || ownIndex < fromIndex)) {
												myTextRemoveEvent.setLength(removeEventLength - (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() - removeEventOffset));
												myTextRemoveEvent.setOffset(historyItem.getOffset());
											}
											else if (historyItem instanceof TextRemoveEvent && historyItemOffset + ((TextRemoveEvent) historyItem).getLength() >= removeEventOffset + removeEventLength && (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() != removeEventOffset + removeEventLength || ownIndex < fromIndex)){
												skip = true;
												((TextRemoveEvent)historyItem).setLength(((TextRemoveEvent) historyItem).getLength() + myTextRemoveEvent.getLength());
											}
											else {
												myTextRemoveEvent.setOffset(removeEventOffset + historyItemTextLengthChange);
											}
										} else if (historyItemOffset <= removeEventOffset && (historyItemOffset != removeEventOffset || ownIndex < fromIndex)) {
											myTextRemoveEvent.setLength(removeEventLength + Math.max(historyItemTextLengthChange, -(removeEventOffset + removeEventLength - historyItemOffset)));

										} else {
											historyItem.setOffset(historyItemOffset + removeEventTextLengthChange);
										}
									}
									myHistoryHandler.updateLogicClock(time);
									int removeEventOffset = myTextRemoveEvent.getOffset();
									int removeEventLength = myTextRemoveEvent.getLength();
									
									//For handling received deletion inputs and not retransmitting them
									if (!skip){
										getAreaDocument().flipFilter();
										myJTextArea.replaceRange(null, removeEventOffset, removeEventOffset + removeEventLength);
										getAreaDocument().flipFilter();
									}
								}
							} catch (IllegalArgumentException ae){
								//	ae.printStackTrace();
							} catch (Exception e) {
								//	e.printStackTrace();
							}
						}
					});
				}
			}
		} catch (SocketException sock){
			socketFailed = true;
			//	sock.printStackTrace();
		}catch(Exception e){
			//	e.printStackTrace();
		}
	}
	/**
	 * Returns the underlying document for the textarea. We need this to switch off the filter, to avoid getting repeated input.
	 * @return TextAreaDocument
	 */
	private TextAreaDocument getAreaDocument(){
		return (TextAreaDocument) myJTextArea.getDocument();
	}
	/**
	 * Sorts the eventHistory
	 * @param time
	 * @param senderIndex
	 * @return
	 */
	private ArrayList<MyTextEvent> sortEventHistory(ArrayList<Double> time, int senderIndex){
		ownIndex = myHistoryHandler.getIndex();
		ArrayList<MyTextEvent> historyInterval = myHistoryHandler.eventHistoryFromTo(time.get(ownIndex), myHistoryHandler.getLogicTime(ownIndex), ownIndex);
		Collections.sort(historyInterval, new Comparator<MyTextEvent>() {
			public int compare(MyTextEvent o1, MyTextEvent o2) {
				return o1.getTime().get(ownIndex).compareTo(o2.getTime().get(ownIndex));
			}
		});
		return historyInterval;
	}
}

