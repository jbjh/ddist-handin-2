import java.io.Serializable;
import java.util.ArrayList;

public class MyTextEvent implements Serializable{
	private ArrayList<Double> time = new ArrayList<Double>();
	private int sender;
	
	MyTextEvent(int offset) {
		this.offset = offset;
	}
	private int offset;
	int getOffset() { return offset; }
	
	public ArrayList<Double> getTime(){
		return time;
	}
	/**
	 * 
	 * @param d Clock time to give the event
	 */
	public void setTime(ArrayList<Double> d){
		time = d;
	}
	
	public int getSender(){
		return sender;
	}
	
	public void setOffset(int off){
		offset = off;
	}
	
	public void setSender(int s){
		sender = s;
	}
	public int getChange() {
		return 0;
	}
	
}
