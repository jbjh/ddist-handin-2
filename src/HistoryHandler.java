import java.util.ArrayList;


public class HistoryHandler {
	private ArrayList<MyTextEvent> myTextEventHistoryArrayList = new ArrayList<MyTextEvent>();
	private ArrayList<Double> logicalClockCount = new ArrayList<Double>();
	private int index = 0;
	
	public void setIndex(int i){
		index = i;
		logicalClockCount.add(0, 0.0);
		logicalClockCount.add(1, 0.1);
	}
	
	/**
	 * @param i 1(client) or 0(server)
	 * @return the logical clock timer count
	 */
	public double getLogicTime(int i){
		return logicalClockCount.get(i);
	}

	/**
	 * @return 1(client) or 0(server) 
	 */
	public int getIndex(){
		return index; 
	}
	
	/**
	 * Increases the logical clock counter by 1
	 */
	public synchronized void incrementTime(){
		logicalClockCount.set(index, getLogicTime(index)+1);
	}
	
	/**
	 * compares the 2 Clock counts and sets the max value to the local count
	 * @param paramArray the array to compare to
	 */
	public synchronized void updateLogicClock(ArrayList<Double> param){
		for(int i = 0; i < param.size(); i++){
			logicalClockCount.set(i, Math.max(logicalClockCount.get(i), param.get(i)));
		}
	}
	
	/**
	 * get a partial arrayList based on upper and lower bounds
	 * @param s lower bound
	 * @param e upper bound
	 * @param index
	 * @return an array from s to e with myTextEvents
	 */
	public ArrayList<MyTextEvent> eventHistoryFromTo(double s, double e, int index){
		ArrayList<MyTextEvent> r = new ArrayList<MyTextEvent>();
		synchronized(myTextEventHistoryArrayList){
			for(MyTextEvent a : myTextEventHistoryArrayList){
				double time = a.getTime().get(index);
				if(time > s && time <= e){
					r.add(a);
				}
			}
			return r;
		}
	}
	
	/**
	 * Returns an arrayList holding all events
	 * @return  myTextEventHistoryArrayList
	 */
	public ArrayList<MyTextEvent> getEventHistory(){
		return myTextEventHistoryArrayList;
	}
	
	/**
	 * @return current time
	 */
	public ArrayList<Double> getTime(){
		ArrayList<Double> time_array = new ArrayList<Double>();
		for(int i = 0; i < logicalClockCount.size(); i++){
			time_array.add(i, logicalClockCount.get(i));
		}
		return time_array;
	}
	/**
	 * adds MyTrxtEvents to the queue
	 * @param a MyTextEvents to add
	 */
	public void appendEvent(MyTextEvent a){
		synchronized(myTextEventHistoryArrayList){
			myTextEventHistoryArrayList.add(a);
		}
	}
}
